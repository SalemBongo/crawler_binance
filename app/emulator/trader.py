# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Trading emulator.
# Version:       1.14.11 --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import time

from app.database.db_queries import get_balance, __update_balance, get_ticker
from config import session, log


def trader(ticker: str, cur_to_sell: str, cur_to_buy: str,
           limit_price: float, quantity: float):
    """Define limit price and wait to action. For example, to sell."""

    price = get_ticker(ticker).price
    balance_sell = get_balance(cur_to_sell)
    order = price * quantity  # init order
    if balance_sell < order:
        log.info('[Fail] Need more {}'.format(cur_to_sell))
        return '[Fail] Need more {}'.format(cur_to_sell)
    else:
        while limit_price < price:
            price = get_ticker(ticker).price  # iteration check current price
            time.sleep(3)  # waiting 3 sec.
            session.close()
        else:
            balance_buy = get_balance(cur_to_buy)
            __update_balance(balance_buy, balance_sell, order,
                             cur_to_buy, cur_to_sell)  # update balance
            session.commit()
            session.close()
            log.info('[Done] Transaction complete.')
            return '[Done] Transaction complete.'
