# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Encryptor of data.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import os
from hashlib import sha256


def encryptor(string):
    """Hash-function."""

    return sha256(string.encode()).hexdigest()


def key_generator():
    """Generator of secret keys."""

    return os.urandom(32)
