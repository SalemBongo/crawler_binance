# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Collection of data (ticker, price) to database.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import asyncio
import websockets
import json
from datetime import datetime

from app.database.db_models import BinanceTable
from app.database.db_queries import get_ticker
from config import session, tickers, log


async def get_data(ticker: str):
    """
    Get data from stream of binance.com and collect to database with timestamp,
    if something has changed.
    """

    socket = await websockets.connect(
        'wss://stream.binance.com:9443/ws/{}@aggTrade'.format(ticker.lower()))
    response = await socket.recv()  # waiting response
    data = json.loads(response)  # convert to .json
    if data['p'] != get_ticker(ticker)[-1].price:  # check changes
        try:
            session.add(BinanceTable(ticker=data['s'],
                                     price=data['p'],
                                     timestamp=datetime.now()))
            session.commit()
        except Exception as exc:
            log.error(exc)
    else:
        await asyncio.sleep(1)
    return data


async def init_task_list():
    """Create task-list."""

    for ticker in tickers.values():
        await get_data(ticker)


if __name__ == '__main__':
    """Infinite loop -- collecting data."""

    try:
        while True:
            asyncio.run(init_task_list())
    except Exception as exc:
        log.error(exc)
        session.close()
