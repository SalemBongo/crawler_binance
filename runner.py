# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Apps-runner.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from app.views.dashboard import app


if __name__ == '__main__':
    """Run Flask and Dash apps, port=8050."""

    app.run_server(debug=True)
