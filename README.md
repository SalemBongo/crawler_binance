crawler_binance
---

Description:
* data collection (tickers, prices) from https://binance.com to database (PostgreSQL)
* API (Flask)
* visualization of data (Dash)
* trading emulator

Current version:
1.14.11  --  Python 3.7

Author:
Stanislav Fux  --  https://gitlab.com/SalemBongo  --  salembongo@gmail.com

Copyright:
(c) Stanislav Fux, 2018
