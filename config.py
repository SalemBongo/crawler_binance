# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Set of configurations.
# Version:       1.14.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import logging as log
from dataclasses import dataclass
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from datetime import timedelta

from app.database.encryptor import key_generator


"""Config for logger."""
log.basicConfig(filename='_log.txt',
                level=log.DEBUG,
                format=u'%(filename)s  [LINE: %(lineno)d]#  '
                       u'(levelname)s  [%(asctime)s]  %(message)s')


@dataclass()
class DatabaseConfig:
    """Config for database."""

    old_db: str = 'postgres'
    new_db: str = 'db_binance'
    db_host: str = 'localhost'
    db_port: str = '5432'
    db_user: str = 'postgres'
    db_pass: str = '123'
    old_db_path: str = 'postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(
        db_user, db_pass, db_host, db_port, old_db)
    new_db_path: str = 'postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(
        db_user, db_pass, db_host, db_port, new_db)
    table_binance: str = 'data_binance'
    table_balance: str = 'trader_balance'
    table_users: str = 'users_data'


"""
Config for connection to databases. 
Set isolation level for database creation.
"""
engine_old_db = create_engine(DatabaseConfig.old_db_path,
                              isolation_level='AUTOCOMMIT')
connection_to_default_db = engine_old_db.connect()
engine_new_db = create_engine(DatabaseConfig.new_db_path)
session = Session(bind=engine_new_db)


@dataclass()
class FlaskAppConfig:
    """Config for Flask-app."""

    SQLALCHEMY_DATABASE_URI: str = 'postgresql://{0}:{1}@{2}/{3}'.format(
        DatabaseConfig.db_user, DatabaseConfig.db_pass,
        DatabaseConfig.db_host, DatabaseConfig.new_db)
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False
    DEBUG: bool = True
    # set token for generate cookie and use sessions
    SECRET_KEY: classmethod = key_generator()
    # set session lifetime
    PERMANENT_SESSION_LIFETIME: classmethod = timedelta(hours=1)


"""Values block."""
tickers = {'ETHBTC': 'ETHBTC',
           'ETHUSDT': 'ETHUSDT',
           'BTCUSDT': 'BTCUSDT'}

currency = {'USDT': 'USDT',
            'BTC': 'BTC',
            'ETH': 'ETH'}


"""Tickers-list for dashboard."""
dropdown_list = []
for ticker in tickers:
    dropdown_list.append({'label': ticker, 'value': ticker})


"""Style block."""
colors = {'background': '#111111',
          'text': '#FFFFFF'}
